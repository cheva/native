<div class="container">

    <form method="post" class="form-signin">
        <h2 class="form-signin-heading">Edit your settings</h2>
        <label for="email" class="sr-only">Email address</label>
        <input type="email" id="email" class="form-control" placeholder="Email address" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
        <button class="btn btn-lg btn-red btn-block" type="discard">Discard</button>
    </form>

</div> <!-- /container -->