<?php
/**
 * Dev tools
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * @param mixed $str
 */
function debug($str)
{
    echo "<pre>";
    var_dump($str);
    echo "<hr/>";
    print_r(debug_backtrace());
}