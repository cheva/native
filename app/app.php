<?php
/**
 * Main app file
 */

namespace app;

use app\core\Router;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;

$container = new Container();
$container['logger'] = function ($c) {
    $logger = new Logger('app');
    $logger->pushHandler(new StreamHandler(__DIR__ . '/../app/app.log', Logger::DEBUG));
    $logger->addDebug(date('Y-m-d H:i:s', time()), [$_SERVER['REQUEST_METHOD'], explode('?', $_SERVER['REQUEST_URI'])[0], $_REQUEST]);
    return $logger;
};

session_start();
$router = new Router($container);
$router->run();