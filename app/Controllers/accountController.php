<?php
/**
 * Class AccountController
 */

namespace app\controllers;

use app\core\Controller;

/**
 * Class AccountController
 * @package app\controllers
 */
final class AccountController extends Controller
{

    public function indexAction()
    {
        $this->view->render();
    }

    public function loginAction()
    {
        $this->view->render();
    }

    public function settingsAction()
    {
        $this->view->render();
    }

    public function registerAction()
    {
        $this->view->render();
    }

    public function errorAction()
    {
        $this->view->render();
    }

}