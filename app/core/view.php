<?php
/**
 * Class View
 */

namespace app\core;

use Pimple\Container;

/**
 * Class View
 * @package app\core
 */
final class View
{

    private $container;

    /**
     * View constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     *
     */
    public function render()
    {
        //get and map parsed vars @todo solid
        $vars = [
            'layout' => __DIR__ . '/../views/layouts/default.php', // layout required
            // and other mapped vars
            'title' => ucfirst($this->container['route'] . ' [' . $_SERVER['REQUEST_METHOD'] . '] '),
            'error' => !empty($this->container['error']) ? $this->container['error'] : 'error...',
        ];
        // than process template file
        echo $this->getView($vars);
        // or process templater
    }

    /**
     * @param array $vars
     * @return string
     */
    private function getView(array $vars): string
    {
        $view = __DIR__ . '/../views/' . $this->container['route'] . '.php';
        if (file_exists($view)) {
            // first - render content
            ob_start();
            include $view; // bad idea
            $vars['main'] = ob_get_clean();
            // second - render layout
            ob_start();
            include $vars['layout'];
            return ob_get_clean();
        } else {
            $this->container['logger']->addError('Error: view ' . $view . ' not exists');
            return 'Error';
        }
    }

}