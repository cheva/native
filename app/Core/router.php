<?php
/**
 * Class Router
 */

namespace app\core;

use app\controllers\AccountController;
use Pimple\Container;

/**
 * Class Router
 * @package app\core
 */
final class Router
{
    private $routes, $params, $controller, $action, $actionName;
    private $container;

    /**
     * Router constructor.
     * @param Container $container
     */
    function __construct(Container $container)
    {

        $this->container = $container;
        $routes = parse_ini_file(__DIR__ . "/../config/routes.ini", true);
        foreach ($routes as $route => $params) {
            $this->setRoutes($route, $params);
        }
    }

    /**
     * Set routes
     * @param string $route
     * @param array $params
     */
    public function setRoutes(string $route, array $params)
    {
        $this->routes["#^/$route$#"] = $params;
    }

    /**
     * Set route params
     * @return bool
     */
    public function setParams(): bool
    {
        $url = $_SERVER['REQUEST_URI'];
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $match)) {
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    /**
     * Load and set controller and action
     * @return Router
     */
    public function run(): Router
    {
        if ($this->setParams()) {
            $this->controller = $this->getController(ucfirst($this->params['controller']) . 'Controller');
            $action = $this->getActionName();
            $this->container['route'] = $this->params['controller'] . '/' . $this->action;
            $this->controller->$action();
        } else {
            $this->container['logger']->addError('Error: route not defined: ' . $_SERVER['REQUEST_URI']);
        }
        return $this;
    }

    /**
     * Fabric Load and return controller
     * @param string $controllerName
     * @return Controller
     */
    protected function getController(string $controllerName): Controller
    {
        $controller = '\app\Controllers\\' . $controllerName;
        $controllerFile = __DIR__ . '/../Controllers/' . $controllerName . '.php';
        if (file_exists($controllerFile)) {
            require_once $controllerFile;
            if (class_exists($controller)) {
                return new $controller($this->container);
            } else {
                $this->container['logger']->addError('Error: class ' . $controller . ' not exists');
            };
        } else {
            $this->container['logger']->addError('Error: file ' . $controllerFile . ' not exists');
        }
        return null;
    }

    /**
     * Define action from route and request
     * @return string
     */
    protected function getActionName()
    {
        if (empty($this->actionName)) {
            $method = $_SERVER['REQUEST_METHOD'];
            if (!empty($this->params['action'][$method])) {
                $this->action = $this->params['action'][$method];
                $this->actionName = $this->action . 'Action';
            }
            if (empty($this->actionName) || !method_exists($this->controller, $this->actionName)) {
                // errors
                $message = 'Error: route ' . trim($_SERVER['REQUEST_URI'], '/') . ' [' . $_SERVER['REQUEST_METHOD'] . '] not exists';
                $this->container['error'] = $message;
                $this->container['logger']->addError($message);
                // reset
                $this->action = 'error';
                $this->actionName = 'errorAction';
            }
        }
        return $this->actionName;
    }

}