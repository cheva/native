<?php
/**
 * Class Controller
 */

namespace app\core;

use Pimple\Container;

/**
 * Class Controller
 * @package app\core
 */
abstract class Controller
{

    public $view;
    private $container;

    /**
     * Controller constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->view = new View($container);
    }

    public function indexAction()
    {
        echo __METHOD__ . "<br>"; # if not redefined
    }

    public function errorAction()
    {
        echo __METHOD__ . "<br>"; # if not redefined
    }

}