<?php
/**
 * Entry point
 */

# load vendors
require __DIR__ . '/../vendor/autoload.php';
# dev errors; use xDebug
require __DIR__ . '/../app/lib/dev.php';
# main app
require __DIR__ . '/../app/app.php';
