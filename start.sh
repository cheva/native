#! /bin/bash

clear
composer install

PHP=$(which php)
if [ $? != 0 ] ; then
    echo "Unable to find PHP"
    exit 1
fi

DOCROOT="$(pwd)/public"
HOST=0.0.0.0
PORT=8888
#INIFILE="$(pwd)/server/server.ini"
#ROUTER="$(pwd)/server/router.php"

#$PHP -S $HOST:$PORT -c $INIFILE -t $DOCROOT $ROUTER
$PHP -S $HOST:$PORT -t $DOCROOT
